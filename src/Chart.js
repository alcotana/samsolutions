import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactHighcharts from 'react-highcharts';
import {Card, CardHeader, CardMedia, CardText} from 'material-ui/Card';

class Chart extends Component {

  static propTypes = {
    socket: PropTypes.object.isRequired
  };

  areaChart = {
    chart: {
      type: 'area'
    },
    title: {
      text: 'CPU usage'
    },
    xAxis: {
      labels: {
        enabled: false
      }
    },
    yAxis: {
      ceiling: 100,
      title: {
        text: '%'
      },
      labels: {
        formatter: function () {
          return this.value;
        }
      }
    },
    tooltip: {
      pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
    },
    plotOptions: {
      area: {
        pointStart: 0,
        marker: {
          enabled: false,
          symbol: 'circle',
          radius: 1,
          states: {
            hover: {
              enabled: true
            }
          }
        }
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: 'CPU',
      data: new Array(30).fill(null),
      showInLegend: false
    }]
  };

  constructor(props) {
    super(props);
    this.socket = this.props.socket;
    this.socket.on('received_cpu', this.updateCPU);
  }

  shouldComponentUpdate() {
    return false;
  }

  updateCPU = (params) => {
    // Update chart
    let area = this.refs.area.getChart();
    area.series[0].addPoint([params.load], true, true);
  };

  render() {
    return (
      <Card style={{margin: "20px"}}>
        <CardHeader
          title="Moving Line Chart"
          style={{backgroundColor: "#F5F5F5"}}
          titleStyle={{fontSize: "large"}}
        />
        <CardMedia style={{margin: "16px"}}>
          <ReactHighcharts config={this.areaChart} ref="area"/>
        </CardMedia>
        <CardText>
          Отображает загрузку CPU каждые 3 секунды (последние 30 значений)
        </CardText>
      </Card>
    );
  }
}

export default Chart;
