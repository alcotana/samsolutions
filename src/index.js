import React from 'react';
import {render} from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './App';
import './index.css';

// Needed to listen for touch / tap / click events
injectTapEventPlugin();

// Render the main react component into the root div
render(<App />, document.getElementById('root'));
