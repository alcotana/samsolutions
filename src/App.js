import React, { Component } from 'react';
import io from 'socket.io-client';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import AppBar from 'material-ui/AppBar';
import {List, ListItem} from 'material-ui/List';
import IconTimeline from 'material-ui/svg-icons/action/timeline';
import IconDonut from 'material-ui/svg-icons/action/donut-small';

import Chart from './Chart';

import './App.css';

class App extends Component {

  state = {
    dashboard: 'cpu',
    cpu: [],
    packets: [],
  };

  constructor(props){
    super(props);

    this.socket = io();
    // this.socket.on('received_cpu', this.updateCPU);
    // this.socket.on('received_packets', this.updatePackets);
  }

  handleCPUClick = (params) => {
    this.setState({
      ...this.state,
      dashboard: 'cpu'
    });
  };

  handlePacketsClick = (params) => {
    this.setState({
      ...this.state,
      dashboard: 'packets'
    });
  };

  render() {
    return (
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <div className="app">
        <header className="header">
          <AppBar title="System Metrics Dashboard"/>
        </header>
        <nav className="sidebar">
          <List>
            <ListItem onTouchTap={this.handleCPUClick} primaryText="CPU" leftIcon={<IconTimeline />} />
            <ListItem onTouchTap={this.handlePacketsClick} primaryText="Packets" leftIcon={<IconDonut />} />
          </List>
        </nav>
          <section className="content">
            <Chart socket={this.socket} />
          </section>
      </div>
    </MuiThemeProvider>
    );
  }
}

export default App;
