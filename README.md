# Test Project

This is an example project for SaM Solutions

## Installation

After cloning the repository, install dependencies and build the app:
```sh
yarn install
yarn build
```

Now you can run your local server:
```sh
yarn start
```
Server is located at http://localhost:3001