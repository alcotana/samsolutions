'use strict';

// Modules
const http = require('http');
const express = require('express');
const cors = require('cors');
const socketIO = require('socket.io');

// Server host and port
const serverHost = 'localhost';
const serverPort = 3001;

// Create Server app
const app = express();
const server = http.createServer(app);
const io = socketIO(server);

// Starting Express server
server.listen(serverPort, serverHost, function() {
  console.log(`Server is running on localhost:${serverPort}...`);
});

// Middleware
app.use(cors());
app.use('/', express.static(__dirname + '/build'));

// API Functions
const getCpu = () => ({
  load: Math.floor(Math.random() * 80) + 20 + 1, // 20-100
  time: new Date().toISOString().substr(11, 8)
});

const getPackets = () => ({
  queue: Math.floor(Math.random() * 60) + 1, // 0-60
  sent: Math.floor(Math.random() * 60) + 1,
  time: new Date().toISOString().substr(11, 8)
});

// Socket.io
let connections = [];

io.sockets.on('connection', (socket) => {
  socket.once('disconnect', () => {
    connections.splice(connections.indexOf(socket), 1);
    socket.disconnect();
    console.log('Disconnected: %s. Remained: %s.', socket.id, connections.length)
  });
  connections.push(socket);
  console.log('Connected: %s. Total: %s', socket.id, connections.length);
});

setInterval(() => {
  io.sockets.emit('received_cpu', getCpu());
}, 3 * 1000);

setInterval(() => {
  io.sockets.emit('received_packets', getPackets());
}, 20 * 1000);


